"""API for extracting frames from a video
"""
import os
import cv2 as opencv

from colr import color

def extract_all_frames(vid_dir, vid_name, frame_prefix='ovee_extract'):
  """Isolates all frames from a given video and writes them to the video folder
        with a prefix if provided

  Args:
      vid_dir (str): The location to read the video from
      vid_name (str): The name of the video
      frame_prefix (str, optional): The name to prepend each frame

  Returns:
      None
  """
  vid_path = f'{vid_dir}/{vid_name}'

  print(color(f'Reading {vid_path}...', fore='green', style='bright'))
  print('\n')

  fail, img_list = opencv.VideoCapture(vid_path).read()

  if not fail:
    print(color('Error: Video Capture not successful, try again!.',
                fore='red', style='bright'))
    return None

  print(color(f'Writing frames to {vid_dir}/frames...', fore='yellow'))

  count = 0
  for img in img_list:
    count += 1
    write_path = f'{vid_dir}/frames/{frame_prefix}-{count}'


    opencv.imwrite(f'{write_path}.jpg', img)

def main():
  """Primary interface, receives user input and runs appropriate command if the
        path is confirmed to be correct.

  Returns:
      None
  """
  welcome = color('Welcome', fore='red')
  to_the = color('to the', fore='blue')
  video = color('video', fore='green')
  extractor = color('extractor!', fore='yellow')

  print(f'{welcome} {to_the} {video} {extractor}\n\n')

  read_path = input('Enter the path to the video files: ')

  vid_path = os.path.dirname(os.path.abspath(read_path))
  is_correct = input(color(f'Is this correct "{vid_path}"? (Y/N) ',
                           fore='yellow', style='bright'))
  print('\n')
  if is_correct == 'y' or is_correct == 'Y':
    vid_name = os.path.basename(read_path)
    extract_all_frames(vid_path, vid_name)
    print('\n')
    print(color('Done!', fore='green', style='bright'))
  else:
    print('Operation Cancelled!')

if __name__ == '__main__':
  main()
