# Ovee
OpenCV Video Extraction Example

---

This repo provides early example code of the extraction features from project S.A.D.


## Getting Started
***OpenCV installation instructions cribbed from at Adrian Rosebrock at pyimage search [here](http://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/)***

### Install OPENCV
Install Open CV for your respective system and make sure it's accessible to this code. Also note we are using python 3 here.

### Setup Environment (Recommended)
The easiest way to get started is to download virtualenv and virtualenvwrapper. Why? Because this will isolate the python environment in which this module will run, all packages and configs will *usually* just work easier this way.

1. Install virtualenv and virtualenvwrapper

```
$ pip install virtualenv
 
$ pip install virtualenvwrapper

```

2. Setup virtualenvwrapper (taken from virtualenvwrapper site). This will place all of your envs in one folder for convinience.
```
$ export WORKON_HOME=~/virtenvs
$ source /usr/local/bin/virtualenvwrapper.sh

```

3.

```
$ git clone https://bitbucket.org/hucsresearch/ovee

$ cd ovee

# Create virtual env for ovee on your machine
$ mkvirtualenv ovee

# Set current working to ovee env
$ workon ovee

# Install packages
$ pip install -r requirements

```

### Running project
A basic example using the provided video file.

![example-screenshot](https://bitbucket.org/hucsresearch/ovee/raw/master/ovee-example.png)